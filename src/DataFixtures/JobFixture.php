<?php

namespace App\DataFixtures;

use App\Entity\Job;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class JobFixture extends Fixture
{

    private $descriptions = [
        'At Abbott, we’re committed to helping you live your best possible life through the power of health. For more than 125 years, we’ve brought new products and technologies to the world - in nutrition, diagnostics, medical devices and branded generic pharmaceuticals - that create more possibilities for more people at all stages of life. Today, 99,000 of us are working to help people live not just longer, but better, in the more than 150 countries we serve.',
        'Our client is a private equity owned, leading global manufacturer of branded apparel for the B2B market. The company has a Global distribution network and operates multiple production facilities. Management and Shareholders aim to continuously grow (international). Mutual goal is to further grow the organization both organic and via acquisitions. With this continuing growth and success, they are looking for a strong Group Controller to strengthen the Finance Team. ',
        'Listen willingly and quickly to record information about new technologies. Communicative in Dutch and English. (both spoken and written) Maintaining punctuality and agreements. Teamplayer who can also work independently Quality and improvement are central. Out of the box and customer-center thinking Good contacts with suppliers and customers',
        'Binnen het team Juridische Beroepen zijn wij per 1 januari 2019 op zoek naar een leraar studievaardigheden, sociale vaardigheden en studieloopbaanbegeleiding voor 0,4 fte. Voor de betreffende vakken ontwikkel je een lessenserie, waarbij je een goed beeld hebt wat onze studenten nodig hebben in het toekomstige beroep. Hiervoor ga je in gesprek met de leraren die lesgeven binnen de opleiding, onderhoudt contacten met het bedrijfsleven en het hbo, zodat de lesstof goed aansluit op de praktijk en de vervolgopleiding. Je verzorgt de lessen studievaardigheden voor de eerstejaars studenten en ondersteunt de leraren en de studieloopbaanbegeleiders bij hun werkzaamheden.',
        'Als caissière bij Albert Heijn ben je het visitekaartje van de winkel. Je bent de laatste persoon die de klant ziet, dus je zorgt ervoor dat het afrekenen snel en zorgvuldig verloopt en dat de klanten met een goed gevoel de winkel uitgaan. Voor deze functie moet je minimaal 16 jaar zijn. Je kunt precies werken en je vindt het leuk om met klanten om te gaan. Tenslotte komt jouw beschikbaarheid overeen met de gevraagde werktijden in bovenstaand rooster.',
        'Als junior adviseur / onderzoeker werk je allerlei maatschappelijk thema’s uit. Je kunt daarbij denken aan de positie van koopstarters op de woningmarkt of de verduurzaming van de gebouwde omgeving. Van iedere opdracht leer je wat nieuws.',
        'De organisatie Trigion is in beweging en verandert van een traditionele (man)beveiligingsorganisatie naar een organisatie die Integrale Veiligheidsoplossingen biedt. Naast fysieke dienstverlening, spelen zaken als consultancy expertise, techniek en data intelligentie een steeds grote rol. We zijn in staat om onder één dak complexe integrale oplossingen te bieden aan onze opdrachtgevers. Dit creëren we samen met onze opdrachtgevers en ruim 6.000 collega\'s. Trigion is onderdeel van de Facilicom Group.',
        'Ben jij graag onderweg en heb je affiniteit met goederenvervoer? Dan is de vacature internationaal vrachtwagenchauffeur wat voor jou! Vanuit Genemuiden rijd je naar verschillende mooie landen in Europa. Als internationaal vrachtwagenchauffeur ben je het gezicht van het bedrijf. Naast de verantwoording van het goederenvervoer, is goede communicatie met klanten een onderdeel van je werkzaamheden.',
        'Binnen het team Juridische Beroepen zijn wij per 1 januari 2019 op zoek naar een leraar studievaardigheden, sociale vaardigheden en studieloopbaanbegeleiding voor 0,4 fte. Voor de betreffende vakken ontwikkel je een lessenserie, waarbij je een goed beeld hebt wat onze studenten nodig hebben in het toekomstige beroep. Hiervoor ga je in gesprek met de leraren die lesgeven binnen de opleiding, onderhoudt contacten met het bedrijfsleven en het hbo, zodat de lesstof goed aansluit op de praktijk en de vervolgopleiding. Je verzorgt de lessen studievaardigheden voor de eerstejaars studenten en ondersteunt de leraren en de studieloopbaanbegeleiders bij hun werkzaamheden.',
        'Op dit moment zijn veel touringcarbedrijven op zoek naar gemotiveerde, flexibele en gastvrije chauffeurs. Stichting FSO helpt deze bedrijven een handje. FSO heeft een instroomtraject waarin touringcarchauffeurs opgeleid worden en vervolgens een contract krijgen voor twee jaar van minimaal 32 uur per week. Een unieke kans voor ondernemende, gastgerichte en reislustige mannen en vrouwen die een mooie stap willen zetten naar een dynamische sector.'
    ];
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $job = new Job();
            $job->setTitle('Job title number #'.$i);
            $job->setDescription($this->descriptions[$i]);
            $manager->persist($job);
        }

        $manager->flush();
    }
}
