<?php

namespace App\Controller;

use App\Entity\Job;
use App\Form\JobType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class JobController extends AbstractController
{
    private $manager;

    /**
     * JobController constructor.
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $jobs = $this->manager->getRepository(Job::class)->findAll();

        return $this->render('job/index.html.twig', [
            'jobs' => $jobs
        ]);
    }

    /**
     * @Route("/job/apply/{id}", name="job_apply")
     */
    public function apply($id, \Swift_Mailer $mailer)
    {
        $job = $this->manager->getRepository(Job::class)->find($id);
        $user = $this->getUser();

        $message = (new \Swift_Message('Application of Job: ' . $job->getTitle()))
            ->setFrom('info@faydoo.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    'emails/application.html.twig', [
                        'user' => $user,
                        'job' => $job
                    ],
                'text/html'
            ));

        if($mailer->send($message)) {
            $this->addFlash(
                'success',
                'Application was sent!'
            );
        } else {
            $this->addFlash(
                'danger',
                'Error sending email!'
            );
        }
        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/job/add", name="job_add")
     */
    public function add(Request $request)
    {
        $job = new Job();
        $form = $this->createForm(JobType::class, $job);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($job);
            $this->manager->flush();

            $this->addFlash(
                'success',
                'Job has been added successfully!'
            );

            return $this->redirectToRoute('index');
        }

        return $this->render('job/add.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
