<?php

namespace App\Controller;

use App\Form\UserType;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserController extends AbstractController
{
    /**
     * @Route("/user/add", name="user_add")
     */
    public function add(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $passwordEncoder)
    {
         $user = new User();
         $form = $this->createForm(UserType::class, $user);

         $form->handleRequest($request);
         if ($form->isSubmitted() && $form->isValid()) {
            if(!$manager->getRepository(User::class)
                ->findBy(['identity' =>$user->getIdentity()])
            )
            {
                $password = $passwordEncoder->encodePassword($user, $user->getPassword());
                $user->setPassword($password);

                $manager->persist($user);
                $manager->flush();
                $this->addFlash(
                    'success',
                    'User was added!'
                );
            } else {
                $this->addFlash(
                    'danger',
                    'User already exists with this identity!'
                );

                return $this->render('user/add.html.twig', [
                    'form' => $form->createView()
                ]);
            }
            return $this->redirectToRoute('login');
         }

        return $this->render('user/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/user/edit", name="user_edit")
     */
    public function edit(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = $this->getUser();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);

            $manager->persist($user);
            $manager->flush();
            $this->addFlash(
                'success',
                'User details have been updated!'
            );

            return $this->redirectToRoute('user_edit');
        }

        return $this->render('user/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
