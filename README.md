To install:

Clone from repository: 
``git@gitlab.com:faydoo/symfony-example-project-2018.git`` or ``https://gitlab.com/faydoo/symfony-example-project-2018.git``

``composer install``

.ENV
```
APP_ENV=dev
APP_SECRET=f230bdf7c553b8122bf96ebedb835606
DATABASE_URL=mysql://<username>:<password>@127.0.0.1:3306/<dbname>
MAILER_URL=smtp://smtp.mailtrap.io:2525?encryption=ssl&auth_mode=cram-md5&username=*****&password=*****
```
``bin/console doctrine:database:create``

``bin/console make:migration``              

``bin/console doctrine:migrations:migrate``

``bin/console doctrine:fixtures:load``
