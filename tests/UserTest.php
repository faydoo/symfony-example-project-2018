<?php

namespace App\Tests;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\Form\Test\TypeTestCase;

class UserTest extends TypeTestCase
{
    public function testSubmitValidData()
    {
        $formData = [
            'identity' => 'TestUser',
            'email' => 'test@user.nl',
            'password' => 'pass123'
        ];

        $compareUser = new User();
        $form = $this->factory->create(UserType::class, $compareUser);
        $form->submit($formData);

        $user = new User();
        $user->setIdentity('TestUser');
        $user->setEmail('test@user.nl');
        $user->setPassword('pass123');

        $this->assertTrue($form->isSynchronized());

        $this->assertEquals($user, $compareUser);

        $view = $form->createView();

        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }
}
